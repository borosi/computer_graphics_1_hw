#pragma region
//=============================================================================================
// Szamitogepes grafika hazi feladat keret. Ervenyes 2014-tol.          
// A //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// sorokon beluli reszben celszeru garazdalkodni, mert a tobbit ugyis toroljuk. 
// A beadott program csak ebben a fajlban lehet, a fajl 1 byte-os ASCII karaktereket tartalmazhat. 
// Tilos:
// - mast "beincludolni", illetve mas konyvtarat hasznalni
// - faljmuveleteket vegezni (printf is fajlmuvelet!)
// - new operatort hivni az onInitialization függvényt kivéve, a lefoglalt adat korrekt felszabadítása nélkül 
// - felesleges programsorokat a beadott programban hagyni
// - tovabbi kommenteket a beadott programba irni a forrasmegjelolest kommentjeit kiveve
// ---------------------------------------------------------------------------------------------
// A feladatot ANSI C++ nyelvu forditoprogrammal ellenorizzuk, a Visual Studio-hoz kepesti elteresekrol
// es a leggyakoribb hibakrol (pl. ideiglenes objektumot nem lehet referencia tipusnak ertekul adni)
// a hazibeado portal ad egy osszefoglalot.
// ---------------------------------------------------------------------------------------------
// A feladatmegoldasokban csak olyan gl/glu/glut fuggvenyek hasznalhatok, amelyek
// 1. Az oran a feladatkiadasig elhangzottak ES (logikai AND muvelet)
// 2. Az alabbi listaban szerepelnek:  
// Rendering pass: glBegin, glVertex[2|3]f, glColor3f, glNormal3f, glTexCoord2f, glEnd, glDrawPixels
// Transzformaciok: glViewport, glMatrixMode, glLoadIdentity, glMultMatrixf, gluOrtho2D, 
// glTranslatef, glRotatef, glScalef, gluLookAt, gluPerspective, glPushMatrix, glPopMatrix,
// Illuminacio: glMaterialfv, glMaterialfv, glMaterialf, glLightfv
// Texturazas: glGenTextures, glBindTexture, glTexParameteri, glTexImage2D, glTexEnvi, 
// Pipeline vezerles: glShadeModel, glEnable/Disable a kovetkezokre:
// GL_LIGHTING, GL_NORMALIZE, GL_DEPTH_TEST, GL_CULL_FACE, GL_TEXTURE_2D, GL_BLEND, GL_LIGHT[0..7]
//
// NYILATKOZAT
// ---------------------------------------------------------------------------------------------
// Nev    : <VEZETEKNEV(EK)> <KERESZTNEV(EK)>
// Neptun : <NEPTUN KOD>
// ---------------------------------------------------------------------------------------------
// ezennel kijelentem, hogy a feladatot magam keszitettem, es ha barmilyen segitseget igenybe vettem vagy 
// mas szellemi termeket felhasznaltam, akkor a forrast es az atvett reszt kommentekben egyertelmuen jeloltem. 
// A forrasmegjeloles kotelme vonatkozik az eloadas foliakat es a targy oktatoi, illetve a 
// grafhazi doktor tanacsait kiveve barmilyen csatornan (szoban, irasban, Interneten, stb.) erkezo minden egyeb 
// informaciora (keplet, program, algoritmus, stb.). Kijelentem, hogy a forrasmegjelolessel atvett reszeket is ertem, 
// azok helyessegere matematikai bizonyitast tudok adni. Tisztaban vagyok azzal, hogy az atvett reszek nem szamitanak
// a sajat kontribucioba, igy a feladat elfogadasarol a tobbi resz mennyisege es minosege alapjan szuletik dontes.  
// Tudomasul veszem, hogy a forrasmegjeloles kotelmenek megsertese eseten a hazifeladatra adhato pontokat 
// negativ elojellel szamoljak el es ezzel parhuzamosan eljaras is indul velem szemben.
//=============================================================================================

#define _USE_MATH_DEFINES
#include <math.h>
#include <stdlib.h>

#include <iostream>
#include <fstream>

#if defined(__APPLE__)                                                                                                                                                                                                            
#include <OpenGL/gl.h>                                                                                                                                                                                                            
#include <OpenGL/glu.h>                                                                                                                                                                                                           
#include <GLUT/glut.h>                                                                                                                                                                                                            
#else                                                                                                                                                                                                                             
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__)                                                                                                                                                                       
#include <windows.h>                                                                                                                                                                                                              
#endif                                                                                                                                                                                                                            
#include <GL/gl.h>                                                                                                                                                                                                                
#include <gl/glu.h>                                                                                                                                                                                                               
#include <gl/glut.h>                                                                                                                                                                                                              
#endif          

#pragma endregion
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Innentol modosithatod...

/*
 * TODO: Ket egyenes metszespontjanak kiszamitasa keplettel, ciklus helyett
 */

//--------------------------------------------------------
// 3D Vektor
//--------------------------------------------------------
struct Vector {
	float x, y, z;

	Vector() {
		x = y = z = 0;
	}
	Vector(float x0, float y0, float z0 = 0) {
		x = x0; y = y0; z = z0;
	}
	Vector operator*(float a) {
		return Vector(x * a, y * a, z * a);
	}
	Vector operator+(const Vector& v) {
		return Vector(x + v.x, y + v.y, z + v.z);
	}
	Vector operator-(const Vector& v) {
		return Vector(x - v.x, y - v.y, z - v.z);
	}
	float operator*(const Vector& v) { 	// dot product
		return (x * v.x + y * v.y + z * v.z);
	}
	Vector operator%(const Vector& v) { 	// cross product
		return Vector(y*v.z - z*v.y, z*v.x - x*v.z, x*v.y - y*v.x);
	}
	float Length() { return sqrt(x * x + y * y + z * z); }
};

//--------------------------------------------------------
// Spektrum illetve szin
//--------------------------------------------------------
struct Color {
	float r, g, b;

	Color() {
		r = g = b = 0;
	}
	Color(float r0, float g0, float b0) {
		r = r0; g = g0; b = b0;
	}
	Color operator*(float a) {
		return Color(r * a, g * a, b * a);
	}
	Color operator*(const Color& c) {
		return Color(r * c.r, g * c.g, b * c.b);
	}
	Color operator+(const Color& c) {
		return Color(r + c.r, g + c.g, b + c.b);
	}
};

typedef struct {
	float x;
	float y;
	float time;
} float2;

typedef struct {
	float x;
	float y;
} Speed;


const int screenWidth = 600;	// alkalmazás ablak felbontása
const int screenHeight = 600;

void drawCircle(float u, float v, float r);
float2 getNormalVector(float2 vec);

/*metszéspont parabola CMSpline*/
float px;
float py;
bool found = false;

float2 cps[1200];
Speed speed[1200];
int cps_index = 0;
bool space_is_pressed = false;

/*
 * Parabola 
 */
class Parabola {
private:
	float p;
	float phi;
	float2 M;
	float2 P;
	int plain_index = 0;
	float2 plain_point[1500];
public:
	float2 point[1500];
	int index = 0;
	int intersection_index = 0;

	Parabola() {}
	Parabola(float2 p1, float2 p2, float2 f) {
		float2 dir;
		dir.x = p2.x - p1.x;
		dir.y = p2.y - p1.y;
		float2 N1 = getNormalVector(dir);
		float A1 = N1.x;
		float B1 = N1.y;
		float C1 = N1.x*p1.x + N1.y*p1.y;
		float2 N2 = dir;
		float A2 = N2.x;
		float B2 = N2.y;
		float C2 = N2.x*f.x + N2.y*f.y;

		// ket egyenes metszespontja
		float rb = (A2 / A1)*B1;
		float rc = (A2 / A1)*C1;
		rb = B2 - rb;
		rc = C2 - rc;
		float2 M;
		M.y = rc / rb;
		M.x = (C1 - M.y*B1) / A1;

		P.x = f.x - M.x;
		P.y = f.y - M.y;

		p = sqrtf(P.x*P.x + P.y*P.y);
		phi = acosf(P.y / p);

		(asinf(P.x / p) < 0) ? phi = phi : phi = -phi;
		
		for (float x = -2; x < 2; x += 0.005) {
			float y = (1 / (2 * p))*(x*x);
			plain_point[plain_index].x = x;
			plain_point[plain_index++].y = y;
			float xr = x*cosf(phi) - y*sinf(phi);
			float yr = x*sinf(phi) + y*cosf(phi);
			point[index].x = xr + M.x + P.x / 2;
			point[index].y = yr + M.y + P.y / 2;
			index++;
		}
	}
	
	void draw() {
		glColor3f(1.0, 1.0, 0.0);
		glBegin(GL_TRIANGLE_FAN);
		for (int i = 0; i < index; i++) {
			glVertex2f(point[i].x, point[i].y);
		}
		glEnd();
	}

	void drawTangentLine() {
		float xpl = plain_point[intersection_index].x;
		float ypl = plain_point[intersection_index].y;
		float m = xpl / p;
		glColor3f(0.0, 1.0, 0.0);

		float2 p1;
		float2 p2;
		p1.x = -2;
		p1.y = m*p1.x;
		p2.x = 2;
		p2.y = m*p2.x;

		float2 p1r;
		float2 p2r;

		p1r.x = p1.x*cosf(phi) - p1.y*sinf(phi) + px;
		p1r.y = p1.x*sinf(phi) + p1.y*cosf(phi) + py;

		p2r.x = p2.x*cosf(phi) - p2.y*sinf(phi) + px;
		p2r.y = p2.x*sinf(phi) + p2.y*cosf(phi) + py;

		glBegin(GL_LINE_STRIP);
			glVertex2f(p1r.x, p1r.y);
			glVertex2f(p2r.x, p2r.y);
		glEnd();
	}
};

Parabola p;

class CMSpline {
private:
	float spline_tangent = 0;
	float2 intersection;

	void Hermite(int i) {
		float a0x = cps[i].x;
		float a1x = speed[i].x;
		float a2x1 = 3 * (cps[i + 1].x - cps[i].x) / pow((cps[i + 1].time - cps[i].time), 2.0);
		float a2x2 = (speed[i + 1].x + 2 * speed[i].x) / (cps[i + 1].time - cps[i].time);
		float a2x = a2x1 - a2x2;
		float a3x1 = 2 * (cps[i].x - cps[i + 1].x) / pow((cps[i + 1].time - cps[i].time), 3.0);
		float a3x2 = (speed[i + 1].x + speed[i].x) / pow((cps[i + 1].time - cps[i].time), 2.0);
		float a3x = a3x1 + a3x2;

		float a0y = cps[i].y;
		float a1y = speed[i].y;
		float a2y1 = 3 * (cps[i + 1].y - cps[i].y) / pow(cps[i + 1].time - cps[i].time, 2.0);
		float a2y2 = (speed[i + 1].y + 2 * speed[i].y) / (cps[i + 1].time - cps[i].time);
		float a2y = a2y1 - a2y2;
		float a3y1 = 2 * (cps[i].y - cps[i + 1].y) / pow((cps[i + 1].time - cps[i].time), 3.0);
		float a3y2 = (speed[i + 1].y + speed[i].y) / pow((cps[i + 1].time - cps[i].time), 2.0);
		float a3y = a3y1 + a3y2;

		bool found_intersection = false;
		float t_i = cps[i].time;
		glBegin(GL_POINTS);

		if (i == cps_index - 1)
			glColor3f(1, 0, 0);
		else if (i == 0)
			glColor3f(1, 1, 0);
		else
			glColor3f(0.0, 0.0, 1.0);

		glLineWidth(2);
		for (float t = cps[i].time; t < cps[i + 1].time; t += 0.005) {
			float x = a3x*pow((t - t_i), 3.0) + a2x*pow((t - t_i), 2.0) + a1x*(t - t_i) + a0x;
			float y = a3y*pow((t - t_i), 3.0) + a2y*pow((t - t_i), 2.0) + a1y*(t - t_i) + a0y;
			glVertex2f(x, y);

			for (int i = 0; i < p.index; i++) {
				if (fabs(x - p.point[i].x) <= 0.004 && !found_intersection) {
					if (fabs(y - p.point[i].y) <= 0.004) {
						px = p.point[i].x;
						py = p.point[i].y;
						p.intersection_index = i;
						intersection.x = x;
						intersection.y = y;

						float tx = 3 * a3x*pow((t - t_i), 2.0) + 2 * a2x*(t - t_i) + a1x;
						float ty = 3 * a3y*pow((t - t_i), 2.0) + 2 * a2y*(t - t_i) + a1y;
						spline_tangent = (float)ty / tx;

						found = true;
						found_intersection = true;
					}
				}
			}
		}
		glEnd();
	}
	
	void calculateSpeed() {
		for (int i = 1; i < cps_index - 1; i++) {
			Speed v1;
			v1.x = (cps[i + 1].x - cps[i].x) / (cps[i + 1].time - cps[i].time);
			v1.y = (cps[i + 1].y - cps[i].y) / (cps[i + 1].time - cps[i].time);

			Speed v2;
			v2.x = (cps[i].x - cps[i - 1].x) / (cps[i].time - cps[i - 1].time);
			v2.y = (cps[i].y - cps[i - 1].y) / (cps[i].time - cps[i - 1].time);

			speed[i].x = 0.5*(v1.x + v2.x);
			speed[i].y = 0.5*(v1.y + v2.y);
		}

		float sum = 0;
		for (int i = 0; i < cps_index - 1; i++)
			sum += (cps[i + 1].time - cps[i].time);
		float avg = (float)sum / cps_index;

		Speed lastSpeed1;
		lastSpeed1.x = (cps[0].x - cps[cps_index - 1].x) / avg;
		lastSpeed1.y = (cps[0].y - cps[cps_index - 1].y) / avg;

		Speed lastSpeed2;
		lastSpeed2.x = (cps[cps_index - 1].x - cps[cps_index - 2].x) / (cps[cps_index - 1].time - cps[cps_index - 2].time);
		lastSpeed2.y = (cps[cps_index - 1].y - cps[cps_index - 2].y) / (cps[cps_index - 1].time - cps[cps_index - 2].time);

		speed[cps_index - 1].x = 0.5*(lastSpeed1.x + lastSpeed2.x);
		speed[cps_index - 1].y = 0.5*(lastSpeed1.y + lastSpeed2.y);

		Speed firstSpeed1;
		firstSpeed1.x = (cps[1].x - cps[0].x) / (cps[1].time - cps[0].time);
		firstSpeed1.y = (cps[1].y - cps[0].y) / (cps[1].time - cps[0].time);

		Speed firstSpeed2;
		firstSpeed2.x = (cps[0].x - cps[cps_index - 1].x) / avg;
		firstSpeed2.y = (cps[0].y - cps[cps_index - 1].y) / avg;

		speed[0].x = 0.5*(firstSpeed1.x + firstSpeed2.x);
		speed[0].y = 0.5*(firstSpeed1.y + firstSpeed2.y);
	}

	void closeSpline() {
		if (cps_index > 2) {
			float sum = 0;
			for (int i = 0; i < cps_index - 1; i++)
				sum += (cps[i + 1].time - cps[i].time);
			float avg = (float)sum / cps_index;

			int i = cps_index - 1;
			float a0x = cps[i].x;
			float a1x = speed[i].x;
			float a2x1 = 3 * (cps[0].x - cps[i].x) / pow((avg), 2.0);
			float a2x2 = (speed[0].x + 2 * speed[i].x) / (avg);
			float a2x = a2x1 - a2x2;
			float a3x1 = 2 * (cps[i].x - cps[0].x) / pow((avg), 3.0);
			float a3x2 = (speed[0].x + speed[i].x) / pow((avg), 2.0);
			float a3x = a3x1 + a3x2;

			float a0y = cps[i].y;
			float a1y = speed[i].y;
			float a2y1 = 3 * (cps[0].y - cps[i].y) / pow(avg, 2.0);
			float a2y2 = (speed[0].y + 2 * speed[i].y) / (avg);
			float a2y = a2y1 - a2y2;
			float a3y1 = 2 * (cps[i].y - cps[0].y) / pow((avg), 3.0);
			float a3y2 = (speed[0].y + speed[i].y) / pow((avg), 2.0);
			float a3y = a3y1 + a3y2;

			bool found_intersection = false;
			float t_i = cps[i].time;
			glBegin(GL_POINTS);
			glColor3f(1, 0, 0);
			glLineWidth(2);
			for (float t = cps[i].time; t < cps[i].time + avg; t += 0.005) {
				float x = a3x*pow((t - t_i), 3.0) + a2x*pow((t - t_i), 2.0) + a1x*(t - t_i) + a0x;
				float y = a3y*pow((t - t_i), 3.0) + a2y*pow((t - t_i), 2.0) + a1y*(t - t_i) + a0y;
				glVertex2f(x, y);

				for (int i = 0; i < p.index; i++) {
					if (fabs(x - p.point[i].x) <= 0.004 && !found_intersection) {
						if (fabs(y - p.point[i].y) <= 0.004) {
							px = p.point[i].x;
							py = p.point[i].y;
							p.intersection_index = i;
							intersection.x = x;
							intersection.y = y;

							float tx = 3 * a3x*pow((t - t_i), 2.0) + 2 * a2x*(t - t_i) + a1x;
							float ty = 3 * a3y*pow((t - t_i), 2.0) + 2 * a2y*(t - t_i) + a1y;
							spline_tangent = (float)ty / tx;

							found = true;
							found_intersection = true;
						}
					}
				}
			}
			glEnd();
		}
	}
public:
	CMSpline() {}

	void drawCatmullRomSpline() {
		speed[0].x = 0.005;
		speed[0].y = 0.005;
		speed[cps_index - 1].x = 0.005;
		speed[cps_index - 1].y = 0.005;

		calculateSpeed();

		for (int i = 0; i < cps_index; i++)
			Hermite(i);

		closeSpline();
	}
	
	void drawSplineTangentLine() {
		float m = spline_tangent;

		float2 p1;
		float2 p2;
		p1.x = -2;
		p1.y = m*p1.x;
		p1.x += intersection.x;
		p1.y += intersection.y;

		p2.x = 2;
		p2.y = m*p2.x;
		p2.x += intersection.x;
		p2.y += intersection.y;
	
		glColor3f(1.0, 0.0, 0.0);
		glBegin(GL_LINE_STRIP);
		glVertex2f(p1.x, p1.y);
		glVertex2f(p2.x, p2.y);
		glEnd();

	}
};

CMSpline c;
Color image[screenWidth*screenHeight];	// egy alkalmazás ablaknyi kép

										// Inicializacio, a program futasanak kezdeten, az OpenGL kontextus letrehozasa utan hivodik meg (ld. main() fv.)
void onInitialization() {
	glViewport(0, 0, screenWidth, screenHeight);

	// Peldakent keszitunk egy kepet az operativ memoriaba
	for (int Y = 0; Y < screenHeight; Y++)
		for (int X = 0; X < screenWidth; X++)
			image[Y*screenWidth + X] = Color(0.0, 1.0, 1.0);

}

// Rajzolas, ha az alkalmazas ablak ervenytelenne valik, akkor ez a fuggveny hivodik meg
void onDisplay() {
	glClearColor(0.1f, 0.2f, 0.3f, 1.0f);		// torlesi szin beallitasa
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // kepernyo torles
	// Peldakent atmasoljuk a kepet a rasztertarba
	glDrawPixels(screenWidth, screenHeight, GL_RGB, GL_FLOAT, image);

	if (cps_index >= 3) {
		p = Parabola(cps[0], cps[1], cps[2]);
		p.draw();
	}

	c.drawCatmullRomSpline();

	for (int i = 0; i < cps_index; i++) {
		drawCircle(cps[i].x, cps[i].y, (float)5 / 1000); // r paraméter constant
	}

	if (found) {
		p.drawTangentLine();
		c.drawSplineTangentLine();
	}

	glutSwapBuffers();	// Buffercsere: rajzolas vege
}

#pragma region OpenGL functions
// Billentyuzet esemenyeket lekezelo fuggveny (lenyomas)
void onKeyboard(unsigned char key, int x, int y) {
	if (key == 32) {
		space_is_pressed = true;
		glutPostRedisplay(); // d beture rajzold ujra a kepet
	}

}

// Billentyuzet esemenyeket lekezelo fuggveny (felengedes)
void onKeyboardUp(unsigned char key, int x, int y) {

}

// Eger esemenyeket lekezelo fuggveny
void onMouse(int button, int state, int x, int y) {
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {   // A GLUT_LEFT_BUTTON / GLUT_RIGHT_BUTTON illetve GLUT_DOWN / GLUT_UP		
		float xc = 2.0*x / screenWidth - 1;
		float yc = 2.0*(screenHeight - y) / screenHeight - 1;
		
		if (!space_is_pressed) {
			cps[cps_index].x = xc;
			cps[cps_index].y = yc;
			cps[cps_index++].time = (float)glutGet(GLUT_ELAPSED_TIME) / 100;
		}

		glutPostRedisplay();
	}
}

// Eger mozgast lekezelo fuggveny
void onMouseMotion(int x, int y)
{

}


long lastTime = 0;

float vx = (float)2*0.6*2; //2m/s
float vy = (float)3*0.6*2; //3m/s
float dx = 0;
float dy = 0;

typedef enum {
	up,
	down,
	left,
	right,
	noDir
} speedDirection;

speedDirection sDir = noDir;

// `Idle' esemenykezelo, jelzi, hogy az ido telik, az Idle esemenyek frekvenciajara csak a 0 a garantalt minimalis ertek
void onIdle() {
	long time = glutGet(GLUT_ELAPSED_TIME);		// program inditasa ota eltelt ido

	if (space_is_pressed) {
			for (long i = 0; i < time - lastTime; i++){
				switch (sDir) {
				case up:
					vy = -vy;
					sDir = noDir;
					break;
				case down:
					vy = -vy;
					sDir = noDir;
					break;
				case left:
					vx = -vx;
					sDir = noDir;
					break;
				case right:
					vx = -vx;
					sDir = noDir;
					break;
				default:
					break;
				}

				dx += vx/1000;
				dy += vy/1000;

				if (dy >= 300)
					sDir = up;
				else if (dy <= -300)
					sDir = down;
				else if (dx >= 300)
					sDir = right;
				else if (dx <= -300)
					sDir = left;
			}
			glViewport(-(screenWidth / 2) - dx, -(screenHeight / 2) - dy, 2 * screenWidth, 2 * screenHeight);
			glutPostRedisplay();
			lastTime = glutGet(GLUT_ELAPSED_TIME);
	}
}
#pragma endregion
/*
	SAJAT FUGGVENYEK
*/
void drawCircle(float u, float v, float r) {
	glColor3f(255, 0, 0);
	glBegin(GL_POLYGON);
	for (int phi = 0; phi < 720; phi++) {
		float x = u + r*cosf(phi * 3.14 / 180);
		float y = v + r*sinf(phi * 3.14 / 180);
		glVertex2f(x, y);
	}
	glEnd();
	glColor3f(255, 255, 255);
	glBegin(GL_LINE_STRIP);
	for (int phi = 0; phi < 720; phi++) {
		float x = u + r*cosf(phi * 3.14 / 180);
		float y = v + r*sinf(phi * 3.14 / 180);
		glVertex2f(x, y);
	}
	glEnd();
}

float2 getNormalVector(float2 vec) {
	float2 n;
	if (vec.y != 0) {
		n.x = -1 * vec.y;
		n.y = vec.x;
	}
	else {
		n.x = vec.y;
		n.y = -1 * vec.x;
	}
	return n;
}

// ...Idaig modosithatod
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#pragma region 
// A C++ program belepesi pontja, a main fuggvenyt mar nem szabad bantani
int main(int argc, char **argv) {
	glutInit(&argc, argv); 				// GLUT inicializalasa
	glutInitWindowSize(600, 600);			// Alkalmazas ablak kezdeti merete 600x600 pixel 
	glutInitWindowPosition(100, 100);			// Az elozo alkalmazas ablakhoz kepest hol tunik fel
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);	// 8 bites R,G,B,A + dupla buffer + melyseg buffer

	glutCreateWindow("Grafika hazi feladat");		// Alkalmazas ablak megszuletik es megjelenik a 	glMatrixMode(GL_MODELVIEW);				// A MODELVIEW transzformaciot egysegmatrixra inicializaljuk
	glLoadIdentity();
	glMatrixMode(GL_PROJECTION);			// A PROJECTION transzformaciot egysegmatrixra inicializaljuk
	glLoadIdentity();

	onInitialization();					// Az altalad irt inicializalast lefuttatjuk

	glutDisplayFunc(onDisplay);				// Esemenykezelok regisztralasa
	glutMouseFunc(onMouse);
	glutIdleFunc(onIdle);
	glutKeyboardFunc(onKeyboard);
	glutKeyboardUpFunc(onKeyboardUp);
	glutMotionFunc(onMouseMotion);

	glutMainLoop();					// Esemenykezelo hurok

	return 0;
}
#pragma endregion
